package com.charitable.main.report.controller;

import com.charitable.main.report.core.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
public class ReportController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("")
    public String landingPage() {
        return "report/home";
    }

    @GetMapping("/report")
    public String getReport(Model model){
        List reportList = restTemplate.getForObject("http://CHARITABLE-REPORT/all", List.class);
        model.addAttribute("reports", reportList);
        return "report/allReports";
    }

}

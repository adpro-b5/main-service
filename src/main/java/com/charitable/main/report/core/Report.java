package com.charitable.main.report.core;

public class Report {
    private int totalDonation;
    protected String name, image;

    public int getTotalDonation() {
        return totalDonation;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

}

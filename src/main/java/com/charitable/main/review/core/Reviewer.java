package com.charitable.main.review.core;

public class Reviewer {
    private String name;
    private String headline;
    private String body;
    private Integer star;
    private long id;

    public Reviewer() {

    }

    public Reviewer(String nama, String title, Integer star, String body) {
        this.name = nama;
        this.headline = title;
        this.star = star;
        this.body = body;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

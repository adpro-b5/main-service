package com.charitable.main.review.controller;

import com.charitable.main.review.core.Reviewer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(path = "/review")
public class ReviewController {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping(path = "")
    public String displayReview(Model model) {
        List<Reviewer> reviewerList = restTemplate.getForObject("http://CHARITABLE-REVIEW/review", List.class);
        model.addAttribute("list", reviewerList);
        return "review/home";
    }

    @GetMapping(path = "/detail/{idReview}")
    public String getArticle(@PathVariable(value="idReview") Long idReview, Model model) {
        String id = String.valueOf(idReview);
        String url = "http://CHARITABLE-REVIEW/review/detail/" + id;

        Reviewer review = restTemplate.getForObject(url, Reviewer.class);
        model.addAttribute("review", review);
        return "review/view-review";
    }

    @GetMapping(path = "/add")
    public String getFormAddReview(Model model) {
        Reviewer reviewer = restTemplate.getForObject("http://CHARITABLE-REVIEW/review/add", Reviewer.class);
        model.addAttribute("reviewer", reviewer);
        return "review/add-review";
    }

    @PostMapping(path = "/add-reviewer")
    public String addReview(@ModelAttribute("reviewer") Reviewer reviewer) {
        HttpEntity<Reviewer> request = new HttpEntity<>(reviewer);
        restTemplate.postForObject("http://CHARITABLE-REVIEW/review/add-reviewer", request, Reviewer.class);
        return "redirect:/review";
    }
}

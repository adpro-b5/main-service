package com.charitable.main.article.core;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsComment {
    private String comment;
    private Integer article;

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getArticle() {
        return this.article;
    }

    public void setArticle(Integer article) {
        this.article = article;
    }
}

package com.charitable.main.article.core;

public class Comment {

    private Long id;
    private String commented;
    private Writer article;

    public Comment() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public void setCommented(String comment) {
        this.commented = comment;
    }

    public String getCommented() {
        return this.commented;
    }

    public void setArticle(Writer article){
        this.article = article;
    }

    public Writer getArticle() {
        return this.article;
    }
}

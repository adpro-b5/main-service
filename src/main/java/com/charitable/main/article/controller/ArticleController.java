package com.charitable.main.article.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import java.util.List;

import com.charitable.main.article.core.Writer;

@Controller
@RequestMapping(path = "/article")
public class ArticleController {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping(path = "")
    public String allArticle(Model model) {
        List<Writer> list = restTemplate.getForObject("http://CHARITABLE-ARTICLE/article", List.class);
        model.addAttribute("list", list);
        return "article/article";
    }

    @GetMapping(path = "/detail/{idArticle}")
    public String getArticle(@PathVariable(value="idArticle") Long idArticle, Model model) {
        String id = String.valueOf(idArticle);
        String url = "http://CHARITABLE-ARTICLE/detail/" + id;

        List<List<String>> article = restTemplate.getForObject(url, List.class);
        List<String> result = article.get(0);
        model.addAttribute("fullname", result.get(0));
        model.addAttribute("title", result.get(1));
        model.addAttribute("email", result.get(2));
        model.addAttribute("body", result.get(3));
        model.addAttribute("id", result.get(4));
        model.addAttribute("listComment", article.get(1));
        return "article/view-article";
    }

    @GetMapping(path =  "/add")
    public String formAddArticle(Model model) {
        model.addAttribute("writer", new Writer());
        return "article/add-article";
    }

    @PostMapping(path = "/add-writer")
    public String addArticle(@ModelAttribute("writer") Writer writer) {
        HttpEntity<Writer> request = new HttpEntity<>(writer);
        restTemplate.postForObject("http://CHARITABLE-ARTICLE/add-article", request, Writer.class);
        return "redirect:/article";
    }

}
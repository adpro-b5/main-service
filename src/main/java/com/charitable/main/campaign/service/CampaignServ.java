package com.charitable.main.campaign.service;

import com.charitable.main.campaign.core.Campaign;

//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
public interface CampaignServ {

    int getEduLike();
    int getSocialLike();
    int getHealthLike();
    void setCampaignLike(String type, boolean naik);
    Campaign getCampaignByType(String type);
}

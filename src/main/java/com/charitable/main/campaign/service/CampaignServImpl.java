package com.charitable.main.campaign.service;

import com.charitable.main.campaign.core.Campaign;
import org.springframework.stereotype.Service;

@Service
public class CampaignServImpl implements CampaignServ {

    private Campaign eduCampaign;
    private Campaign socialCampaign;
    private Campaign healthCampaign;

    public CampaignServImpl() {
        eduCampaign = new Campaign();
        socialCampaign =  new Campaign();
        healthCampaign = new Campaign();
    }

    @Override
    public int getEduLike() {
        return eduCampaign.getLike();
    }

    @Override
    public int getSocialLike() {
        return socialCampaign.getLike();
    }

    @Override
    public int getHealthLike() {
        return healthCampaign.getLike();
    }

    @Override
    public void setCampaignLike(String type, boolean naik) {
        Campaign campaign = getCampaignByType(type);
        campaign.addLike(naik);
    }

    @Override
    public Campaign getCampaignByType(String type) {
        if(type.equalsIgnoreCase("educational")) {
            return eduCampaign;
        } else if (type.equalsIgnoreCase("social")) {
            return socialCampaign;
        } else if (type.equalsIgnoreCase("health")) {
            return healthCampaign;
        }
        return null;
    }
}

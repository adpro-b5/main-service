package com.charitable.main.campaign.controller;

import com.charitable.main.campaign.service.CampaignServ;
import com.charitable.main.campaign.core.Donation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequestMapping(path = "/campaign")
public class CampaignController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CampaignServ campaignServ;

    @GetMapping(path = "")
    public String allCampaign(Model model) {
        List<String> campaignList = restTemplate.getForObject(
                "http://CHARITABLE-CAMPAIGN/home",
                List.class);

        Integer eduLike = campaignServ.getEduLike();

        Integer socialLike = campaignServ.getSocialLike();

        Integer healthLike = campaignServ.getHealthLike();

        model.addAttribute("eduCampaign", campaignList.get(0));
        model.addAttribute("socialCampaign", campaignList.get(1));
        model.addAttribute("healthCampaign", campaignList.get(2));
        model.addAttribute("eduLike", eduLike);
        model.addAttribute("socialLike", socialLike);
        model.addAttribute("healthLike", healthLike);
        return "campaign/allCampaign";
    }

    @GetMapping(path = "/edu")
    public String eduCampaign(Model model) {
        Iterable educationalCampaigns = restTemplate.getForObject(
                "http://CHARITABLE-CAMPAIGN/detail/edu",
                Iterable.class);
        Integer like = campaignServ.getEduLike();
        model.addAttribute("type", "Educational");
        model.addAttribute("image", "educational");
        model.addAttribute("campaign", educationalCampaigns);
        model.addAttribute("like", like);
        return "campaign/detailCampaign";
    }

    @GetMapping(path = "/health")
    public String healthCampaign(Model model) {
        Iterable educationalCampaigns = restTemplate.getForObject(
                "http://CHARITABLE-CAMPAIGN/detail/health",
                Iterable.class);
        Integer like = campaignServ.getHealthLike();
        model.addAttribute("type", "Health");
        model.addAttribute("image", "health");
        model.addAttribute("campaign", educationalCampaigns);
        model.addAttribute("like", like);
        return "campaign/detailCampaign";
    }

    @GetMapping(path = "/social")
    public String socialCampaign(Model model) {
        Iterable educationalCampaigns = restTemplate.getForObject(
                "http://CHARITABLE-CAMPAIGN/detail/social",
                Iterable.class);
        Integer like = campaignServ.getSocialLike();
        model.addAttribute("type", "Social");
        model.addAttribute("image", "social");
        model.addAttribute("campaign", educationalCampaigns);
        model.addAttribute("like", like);
        return "campaign/detailCampaign";
    }

    @RequestMapping(path = "/like/{type}/{naik}")
    public void likeCampaign(Model model, @PathVariable(value = "type") String type,
                             @PathVariable(value = "naik") boolean naik) {
        String[] parts = type.split(" ");
        String tipe = parts[0];
        campaignServ.setCampaignLike(tipe,naik);
    }

    @GetMapping(path = "/{type}/pay")
    public String paymentCampaign(@PathVariable(value = "type") String type, Model model) {
        Donation donation = restTemplate.getForObject(
                "http://CHARITABLE-CAMPAIGN/create-donation/" + type,
                Donation.class);

        model.addAttribute("type", type);
        model.addAttribute("image", type.toLowerCase());
        model.addAttribute("donation", donation);

        return "campaign/payCampaign";
    }

    @PostMapping(path = "/confirm-payment")
    public String confirmPayment(@ModelAttribute("donation") Donation donation) {
        HttpEntity<Donation> request = new HttpEntity<>(donation);
        restTemplate.postForObject("http://CHARITABLE-CAMPAIGN/confirm-donation",
                request, Donation.class);
        return "redirect:/campaign";
    }
}

package com.charitable.main.campaign.core;

public class Campaign {

    protected int like;

    public Campaign() {
        this.like = 0;
    }

    public int getLike() {
        return like;
    }

    public void addLike(boolean naik) {
        like += naik ? 1 : -1;
    }
}

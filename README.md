# Main Service

Deployed on https://charitable-main.herokuapp.com

# Guide to CharitAble

Access the following links in order:

1. https://charitable-registry.herokuapp.com
2. https://charitable-articles.herokuapp.com
3. https://charitable-campaign.herokuapp.com
4. https://charitable-report.herokuapp.com
5. https://charitable-review.herokuapp.com

Then, you can access our main application on https://charitable-main.herokuapp.com

## Important Links for Microservices
- Registry [Repository](https://gitlab.com/adpro-b5/registry )  [Site](http://charitable-registry.herokuapp.com/)
- Article Service [Repository](https://gitlab.com/adpro-b5/article-deployment)  [Site](http://charitable-articles.herokuapp.com/)
- Campaign Service [Repository](https://gitlab.com/adpro-b5/campaign-service)  [Site](http://charitable-campaign.herokuapp.com/)
- Main Service [Repository](https://gitlab.com/adpro-b5/main-service)  [Site](http://charitable-main.herokuapp.com/)
- Report Service [Repository](https://gitlab.com/adpro-b5/report-service) [Site](https://charitable-report.herokuapp.com/)
- Review Service [Repository](https://gitlab.com/adpro-b5/review-service)  [Site](http://charitable-review.herokuapp.com/)
- User Service [Repository](https://gitlab.com/adpro-b5/user-service)  Site